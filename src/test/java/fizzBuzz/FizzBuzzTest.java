package fizzBuzz;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class FizzBuzzTest {
    @Test
    public void fizz3() {
    	assertTrue(FizzBuz.checkFizz(3));
    }
    
    @Test
    public void fizz9() {
    	assertTrue(FizzBuz.checkFizz(9));
    }
    
    @Test
    public void fizz15() {
    	assertTrue(FizzBuz.checkFizzBuzz(15));
    }
    
    @Test
    public void buzz5() {
    	assertTrue(FizzBuz.checkBuzz(5));
    }
    
    @Test
    public void buzz10() {
    	assertTrue(FizzBuz.checkBuzz(10));
    }
    
    @Test
    public void buzz15() {
    	assertTrue(FizzBuz.checkBuzz(15));
    }
    
    @Test
    public void fizzPrinter15() {
    	assertEquals(FizzBuzzPrinter.fizzBuzzPrinter(15) , "FizzBuzz");
    }
    
    @Test
    public void fizzPrinter3() {
    	assertEquals(FizzBuzzPrinter.fizzBuzzPrinter(3), "Fizz");
    }
    
    @Test 
    public void fizzPrinter5() {
    	assertEquals(FizzBuzzPrinter.fizzBuzzPrinter(5), "Buzz");
    }
}
